package Battleship.Players

import Battleship._

import scala.util.Random

trait Player {
    val name: String
    val ships: List[Ship]
    val mesh: Mesh
    val random: Random

    def isAlive: Boolean = {
        val numberLifePoint: Int = ships.map(_.typeShip.size).sum
        this.mesh.listTouchedCell().length != numberLifePoint
    }


    def createShips(typeShips: List[TypeShip], f1:() => Int, f2:() => Int, f3:() => Int): Player

    def shot(cell: Cell, gameState: GameState): GameState = {

        val resultShot: TypeCell.Value = gameState.player2.mesh.checkCell(cell)

        val newShips: List[Ship] = resultShot match {
            case TypeCell.OCCUPIED | TypeCell.TOUCHED =>
                gameState.player2.ships.map(ship => {
                    if (ship.cells.contains(cell.copy(typeCell = TypeCell.OCCUPIED)) || ship.cells.contains(cell.copy(typeCell = TypeCell.TOUCHED))) {
                        val tempShip: Ship = ship.hit(cell)
                        if (this.isHuman){
                            Show.show("You hitted a ship !\n")
                            if (tempShip.isSunk) {
                                Show.show(s"Well done ! The ${tempShip.typeShip.name} is sunk.\n")
                            }
                        }
                        tempShip
                    } else ship
                })
            case _ =>
                if(this.isHuman) Show.show("Plouf ! You missed your shot\n")
                gameState.player2.ships
        }

        val player2AfterFirstShot: Player = gameState.player2.copyShips(ships = newShips)
        val newGridPlayer2: Mesh = player2AfterFirstShot.mesh.shot(cell)
        val newPlayer2: Player = player2AfterFirstShot.copyShipsAndGrid(player2AfterFirstShot.ships, newGridPlayer2)
        GameState(newPlayer2, gameState.player1)
    }

    def copyShips(ships: List[Ship]): Player
    def copyShipsAndGrid(ships: List[Ship], grid: Mesh): Player
    def updateInformation(): Player
    def getInfoForShot(opponentPlayer: Player): Cell
    def isHuman: Boolean
}
