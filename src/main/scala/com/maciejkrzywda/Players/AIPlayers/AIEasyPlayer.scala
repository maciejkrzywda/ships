package com.maciejkrzywda.Players.AIPlayers

import Battleship.Players.Player
import Battleship._

import scala.util.Random

case class AIEasyPlayer(name: String = "AI Random Player", ships: List[Ship] = List(), mesh: Mesh = Mesh.generateMesh(), random: Random = new Random) extends AI {

  override def copyShips(newShips: List[Ship]): Player = {
    AIEasyPlayer(this.name, ships = newShips, this.mesh, this.random)
  }

  override def copyShipsAndGrid(ships: List[Ship], mesh: Mesh): Player = {
    AIEasyPlayer(this.name, ships, mesh, this.random)
  }

  override def getInfoForShot(opponentPlayer: Player): Cell = {
    Cell(this.random.nextInt(Settings.MESH_SIZE), this.random.nextInt(Settings.MESH_SIZE), TypeCell.NON)
  }

    override def createShips(typeShips: List[TypeShip], f1:() => Int, f2:() => Int, f3:() => Int): Player = {
        if (typeShips.isEmpty) {
            AIEasyPlayer(this.name, this.ships, this.mesh, this.random)
        } else {
            val firstTypeShip: TypeShip = typeShips.head
            val letter = f1()
            val number = f2()
            val direction = f3()
            val cell = Cell(letter, number, TypeCell.OCCUPIED)
            val tempShip : Ship = Ship.createShip(firstTypeShip, cell, direction)

            if (this.mesh.checkPosition(tempShip)) {
                val newMesh: Mesh = this.mesh.placeShip(tempShip)
                val newListShips: List[Ship] = tempShip :: this.ships
                val newPlayer: AI = AIEasyPlayer(this.name, newListShips, newMesh, this.random)
                val newTypeShips: List[TypeShip] = typeShips.tail
                newPlayer.createShips(newTypeShips, f1, f2, f3)
            } else {
                this.createShips(typeShips, f1, f2, f3)
            }
        }
    }
}
