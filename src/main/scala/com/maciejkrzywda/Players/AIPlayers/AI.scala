package com.maciejkrzywda.Players.AIPlayers

import Battleship.Settings
import Battleship.Players.Player


trait AI extends Player{

    override def updateInformation(): Player = {
        this.createShips(Settings.TYPESHIP,
            () => random.nextInt(Settings.MESH_SIZE),
            () => random.nextInt(Settings.MESH_SIZE),
            () => random.nextInt(Settings.NB_DIRECTION))
    }

    override def isHuman: Boolean = false
}
