package Battleship.Players

import Battleship._

import scala.io.StdIn
import scala.util.Random

case class HumanPlayer(name: String = "", ships: List[Ship] = List(), mesh: Mesh = Mesh.generateMesh(), random: Random = new Random) extends Player {
    
    override def isHuman: Boolean = true

    override def copyShips(newShips: List[Ship]): Player = {
        HumanPlayer(this.name, ships = newShips, this.mesh, this.random)
    }

    override def copyShipsAndGrid(ships: List[Ship], grid: Mesh): Player = {
        HumanPlayer(this.name, ships, grid, this.random)
    }

    override def updateInformation(): Player = {
            val player: HumanPlayer = HumanPlayer(namePlayer)
            player.createShips(Settings.TYPESHIP,
                () => Input.chooseLetter(Settings.TEXT_POSITIONING_SHIP),
                () => Input.chooseNumber(Settings.TEXT_POSITIONING_SHIP),
                () => Input.chooseDirection())
    }

    override def getInfoForShot(opponentPlayer: Player): Cell = {
      val letter = Input.chooseLetter(Settings.TEXT_SHOOT)
        val number = Input.chooseNumber(Settings.TEXT_SHOOT)
        Cell(letter, number, TypeCell.NON)
    }
    
    override def createShips(typeShips: List[TypeShip], f1:() => Int, f2:() => Int, f3:() => Int): Player = {
        if (typeShips.isEmpty) {
            HumanPlayer(this.name, this.ships, this.mesh)
        } else {
            val firstTypeShip: TypeShip = typeShips.head
            Show.show(s"You have to place the ship ${firstTypeShip.name}, it has a size of ${firstTypeShip.size} cells.")
            val letter = f1()
            val number = f2()
            val direction = f3()
            val cell = Cell(letter, number, TypeCell.OCCUPIED)
            val tempShip : Ship = Ship.createShip(firstTypeShip, cell, direction)

            if (this.mesh.checkPosition(tempShip)) {
                val newGrid: Mesh = this.mesh.placeShip(tempShip)
                val newListShips: List[Ship] = tempShip :: this.ships
                val newPlayer: HumanPlayer = HumanPlayer(this.name, newListShips, newGrid)
                val newTypeShips: List[TypeShip] = typeShips.tail
                newPlayer.createShips(newTypeShips, f1, f2, f3)
            } else {
                Show.show("Not good positioned on the grid. Please try again.\n")
                this.createShips(typeShips, f1, f2, f3)
            }
        }
    }
}
