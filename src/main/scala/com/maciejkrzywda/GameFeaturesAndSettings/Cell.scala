package Battleship

case class Cell(x: Int, y: Int, typeCell: TypeCell.Value) {}

object TypeCell extends Enumeration {
    val WATER, OCCUPIED, TOUCHED, NON = Value
}
