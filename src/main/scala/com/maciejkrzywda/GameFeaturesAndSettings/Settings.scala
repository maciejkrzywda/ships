package Battleship

object Settings {

    val TEXT_POSITIONING_SHIP: String = "Kapitanie, gdzie rozmieścić naszą flote?"
    val TEXT_SHOOT: String = "Kapitanie, gdzie strzelać??"

    val MESH_SIZE: Int = 10
    val NB_FIGHTS_AI: Int = 100
    val NB_DIRECTION: Int = 4


      /*
      Rordzaje statków
      https://www.hasbro.com/common/instruct/Battleship.PDF
       */
    val TYPESHIP: List[TypeShip] = List(TypeShip("Carrier",5),
        TypeShip("Battleship",4),
        TypeShip("Cruiser",3),
        TypeShip("Submarine",3),
        TypeShip("Destroyer",1))

}
