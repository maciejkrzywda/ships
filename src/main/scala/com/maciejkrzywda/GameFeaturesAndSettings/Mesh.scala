package Battleship

import scala.annotation.tailrec

case class Mesh(cells: List[List[Cell]] = List(List())) {
    def checkPosition(tempShip: Ship): Boolean = {
        tempShip.cells.count(cell => {
            if (cell.x < Settings.MESH_SIZE && cell.x >= 0 && cell.y < Settings.MESH_SIZE && cell.y >= 0) {
                this.cells(cell.x)(cell.y).typeCell match {
                    case TypeCell.OCCUPIED => false
                    case _ => true
                }
            } else false
        }) == tempShip.cells.length
    }
    def placeShip(ship: Ship): Mesh = {
        @tailrec
        def placeCellOnMesh(cells: List[Cell], grid: Mesh) : Mesh = {
            if (cells.isEmpty) grid
            else {
                val cell: Cell = cells.head
                placeCellOnMesh(cells.tail, Mesh(grid.cells.updated(cell.x, grid.cells(cell.x).updated(cell.y, cell))))
            }
        }

        placeCellOnMesh(ship.cells, this)

    }

    def shot(cell: Cell): Mesh = {
        this.cells(cell.x)(cell.y).typeCell match {
            case TypeCell.TOUCHED => this
            case TypeCell.OCCUPIED =>
                val newCell = Cell(cell.x, cell.y, TypeCell.TOUCHED)
                Mesh(this.cells.updated(cell.x, this.cells(cell.x).updated(cell.y, newCell)))
            case TypeCell.NON =>
                val newCell = Cell(cell.x, cell.y, TypeCell.WATER)
                Mesh(this.cells.updated(cell.x, this.cells(cell.x).updated(cell.y, newCell)))
            case TypeCell.WATER => this
        }
    }

    def checkCell(cell: Cell): TypeCell.Value = {
        this.cells(cell.x)(cell.y).typeCell
    }

    def listTouchedCell(): List[Cell] = {
        this.cells.flatten.filter(cell => cell.typeCell == TypeCell.TOUCHED)
    }

}

object Mesh {

    def generateMesh() : Mesh = {

        @tailrec
        def generateCells(x: Int, listCells: List[List[Cell]]) : List[List[Cell]] = {
            if (x> Settings.MESH_SIZE-1) listCells
            else {
              generateCells(x+1,  listCells :+ createColumn(x,0, List()))
            }
        }
        @tailrec
        def createColumn(x: Int, y: Int, cells: List[Cell]): List[Cell] = {
            if (y > Settings.MESH_SIZE-1) cells
            else {
                createColumn(x, y+1, cells :+ Cell(x,y,TypeCell.NON) )
            }
        }

        Mesh(generateCells(0, List()))
    }
}