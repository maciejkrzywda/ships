package Battleship

import scala.annotation.tailrec

case class TypeShip(name: String, size: Int){}

case class Ship(typeShip: TypeShip, cells: List[Cell]) {
    def hit(cell: Cell): Ship = {
        val index: Int = this.cells.indexWhere(cellBoat => cell.x == cellBoat.x && cell.y == cellBoat.y)
        val newCells: List[Cell] = this.cells.updated(index, cell.copy(typeCell = TypeCell.TOUCHED))
        this.copy(cells = newCells)
    }
    def numberCellsTouched(): Int = {
        this.cells.count(cell => cell.typeCell == TypeCell.TOUCHED)
    }

    def isSunk: Boolean = {
         this.numberCellsTouched() == this.typeShip.size
    }

}


object Ship {

    def createShip(typeShip: TypeShip, cell: Cell, direction: Int) : Ship = {
        val cells: List[Cell] = createListCellsFor(typeShip, cell, direction, List(cell))
        Ship(typeShip, cells)
    }
    @tailrec
    private def createListCellsFor(typeShip: TypeShip, cell: Cell, direction: Int, cells: List[Cell]) : List[Cell] = {
        if (typeShip.size == 1)
            cells
        else {
            val newTypeShip: TypeShip = typeShip.copy(size = typeShip.size-1)
            val newCell: Cell = direction match {
                case 1 => cell.copy(y = cell.y - 1)
                case 2 => cell.copy(x = cell.x + 1)
                case 3 => cell.copy(y = cell.y + 1)
                case _ => cell.copy(x = cell.x - 1)
            }
            createListCellsFor(newTypeShip, newCell, direction, cells :+ newCell)
        }
    }
}