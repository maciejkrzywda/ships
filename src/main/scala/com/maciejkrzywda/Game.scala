package Battleship
import scala.util.Random
import Battleship.Players.{HumanPlayer, Player}
import com.maciejkrzywda.Players.AIPlayers.AIEasyPlayer

case class GameState(player1: Player, player2: Player)
object Game extends App {

    Show.clearPage()
    val r = new Random
    Show.show("Time to play in BATTLESHIP!!\n")
    main(r)

    def main(r: Random) : Unit = {
        val mode = Input.chooseMode()
        mode match {
            case "1" =>
                val player1Empty: Player = HumanPlayer("1")
                val player2Empty: Player = AIEasyPlayer(random = r)

                val gameState: GameState = initiateGame(player1Empty, player2Empty)

                val winner: Player = playGame(gameState)
                Show.show(s"THE WINNER IS ${winner.name} !")

            case _ =>
                Show.show("You're welcome!")
                return
        }
        Show.show("Play again?")
        main(r)
    }

    def initiateGame(player1Empty: Player, player2Empty: Player): GameState = {
        val player1: Player = player1Empty.updateInformation()
        val player2: Player = player2Empty.updateInformation()
        GameState(player1, player2)
    }

    def playGame(gameState: GameState): Player = {
      if (gameState.player1.isAlive){
            val newGameState = playTurn(gameState)
            playGame(newGameState)
        } else {
            gameState.player2
        }
    }
    def playTurn(gameState: GameState): GameState = {
        if (gameState.player1.isHuman){
            Show.clearPage()
            Show.show("your ships\n")
            Show.showMeshShips(gameState.player1.mesh)
            Show.show("    Mesh of your shots\n")
            Show.showCellMeshShot(gameState.player2.mesh)        }

        val cell: Cell = gameState.player1.getInfoForShot(gameState.player2)
        gameState.player1.shot(cell, gameState)
    }
}
