package Battleship


import scala.annotation.tailrec

object Show {

    def show(s: String) : Unit = {
        println(s)
    }
    def showSameLine(s: String): Unit = {
        print(s)
    }
    // http://www.codecodex.com/wiki/Clear_the_console_and_control_attributes
    def clearPage(): Unit = {
        print("\u001b[2J")
    }
    def showMeshShips(grid: Mesh): Unit = {
        def showCellMeshShip(cell: Cell): Unit = {
            cell.typeCell match {
                case TypeCell.WATER => Show.showSameLine(Console.BLUE+"@ "+Console.RESET)
                case TypeCell.TOUCHED => Show.showSameLine(Console.RED+"● "+Console.RESET)
                case TypeCell.NON => Show.showSameLine(Console.GREEN +"□ "+Console.RESET)
                case TypeCell.OCCUPIED => Show.showSameLine(Console.WHITE+"ο "+Console.RESET)
            }
        }

        show("   A B C D E F G H I J")
        showMeshTR(0, 0, grid, showCellMeshShip)
        show("")
    }

    @tailrec
    def showMeshTR(x: Int, y: Int, grid: Mesh, f1: Cell => Unit) : Unit = {
        if(x == 0) {
            Show.showSameLine({y+1}+" ")
            if (y<9) Show.showSameLine(" ")
        }
        if (x < Settings.MESH_SIZE) {
            f1(grid.cells(x)(y))
            showMeshTR(x+1, y, grid, f1)
        }
        else if (y < Settings.MESH_SIZE-1) {
            Show.show("")
            showMeshTR(0, y+1, grid, f1)
        }
    }

    def showCellMeshShot(grid: Mesh): Unit = {
        def showCellMeshShot(cell: Cell): Unit = {
            cell.typeCell match {
                case TypeCell.WATER => Show.showSameLine(Console.BLUE+"@ "+Console.RESET)
                case TypeCell.TOUCHED => Show.showSameLine(Console.RED+"● "+Console.RESET)
                case TypeCell.NON | TypeCell.OCCUPIED => Show.showSameLine(Console.GREEN +"□ "+Console.RESET)
            }
        }
        show("   A B C D E F G H I J")
        showMeshTR(0, 0, grid, showCellMeshShot)
        show("")
    }
}
