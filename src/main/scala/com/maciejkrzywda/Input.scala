package Battleship

import scala.io.StdIn

object Input {

    def chooseMode(): String = {
        Show.show ("Press 1 to play the game.\n" +
            "Press any other key to quit the program.\n")

        StdIn.readLine()
    }

    def chooseLetter(s: String): Int = {
        Show.show(s)
        Show.show ("Put letter between A and J\n")

        val input = StdIn.readLine().toUpperCase()
        if(input.isEmpty) {
            Show.show("Please try again.\n")
            chooseLetter(s)
        } else {
            val letter: Char = input.head
            letter match {
                case 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' => letter.toInt-65
                case _ =>
                    Show.show("Wrong parameter. Please try again.\n")
                    chooseLetter(s)
            }
        }
    }

    def chooseNumber(s: String): Int = {
        Show.show(s)
        Show.show("put number between 1 and 10\n")
        val number = StdIn.readLine()
        number match {
            case "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "10" =>
                number.toInt-1
            case _ =>
                Show.show("YWrong parameter. Please try again.\n")
                chooseNumber(s)
        }
    }

    def chooseDirection(): Int = {
        Show.show("PUT number between 1 and 4.\n" +
            "1 ship toward the top.\n" +
            "2 hip toward the right.\n" +
            "3 ship toward the bottom.\n" +
            "4 ship toward the left.\n")

        val number = StdIn.readLine()
        number match {
            case "1" | "2" | "3" | "4" =>
                number.toInt
            case _ =>
                Show.show("Wrong parameter. Please try again.\n")
                chooseDirection()
        }
    }
}
